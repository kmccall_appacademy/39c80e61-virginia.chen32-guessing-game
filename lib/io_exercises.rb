# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  number = Random.rand(99)+1
  guess = 0
  guesses = []
  until guess == number
    puts "Guess a number"
    guess = gets.chomp.to_i
    guesses.push(guess)
    if guess > number
      puts "#{guess} is too high!"
    elsif guess < number
      puts "#{guess} is too low!"
    end
    puts "You've guessed #{guesses}"
  end
  puts "The number was #{number}. You used #{guesses.length} guesses."
end

def file_shuffler
  puts "Give me a file"
  file = gets.chomp
  contents = File.readlines(file)
  scrambled = File.new("#{file}-shuffled.txt","w")
    lines = contents.shuffle
    lines.each do |line|
      scrambled.puts "#{line}"
    end
  scrambled.close
end

if $PROGRAM_NAME == __FILE__
  file_shuffler
end
